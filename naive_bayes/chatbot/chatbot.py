import nltk
from sklearn.naive_bayes import MultinomialNB


class Engine(object):
    MIN_WORD_SIZE = 2

    def __init__(self, intents):
        self.intents = intents
        self.dictionary_words = self.assemble_dictionary()
        self.bayes = MultinomialNB()

    def predict(self, dialog):
        x_train = []
        y_train = []
        for intent in self.intents:
            x_train.append(self.vectorize_phrase(self.clean_vector_words(intent.phrases)))
            y_train.append(intent.name)

        self.bayes.fit(x_train, y_train)
        x = [self.vectorize_phrase(self.clean_vector_words(dialog))]
        y = self.bayes.predict(x)
        return y

    def assemble_dictionary(self):
        words = set()
        for intent in self.intents:
            words.update(self.clean_vector_words(intent.phrases))
        return self.indexing_words(words)

    def indexing_words(self, words):
        return {word: index for word, index in zip(words, range(len(words)))}

    def clean_vector_words(self, phrase):
        # ntk.download('rspl')
        # ntk.download('punkt')
        # portuguese suffix remover
        stemmer = nltk.stem.RSLPStemmer()
        words = []
        for word in phrase:
            words.extend(nltk.tokenize.word_tokenize(word.lower()))
        return [stemmer.stem(word) for word in words if len(word) > Engine.MIN_WORD_SIZE]

    def vectorize_phrase(self, words):
        vector = [0] * len(self.dictionary_words)
        for word in words:
            if word in self.dictionary_words:
                position = self.dictionary_words[word]
                vector[position] += 1
        return vector


class Dialog(object):
    def __init__(self, dialogs, intent):
        self.dialogs = dialogs
        self.intent = intent

    def __str__(self):
        return 'Dialogs: {0}, [{1}]'.format([str(i) for i in self.dialogs], self.intent)


class Intent(object):
    def __init__(self, name, phrases):
        self.name = name
        self.phrases = phrases

    def __str__(self):
        return 'Intent: {0}, Phrases: {1}'.format(self.name, str(self.phrases))


class Bot(object):
    def __init__(self, name, engine):
        self.name = name
        self.engine = engine
        self.dialogs = []

    def add_dialog(self, dialog):
        self.dialogs.append(dialog)

    def dialog(self, text):
        predict = self.engine.predict([text])
        for dialog in self.dialogs:
            if predict == dialog.intent:
                return print(dialog)

    def __str__(self):
        return 'Bot: {0}\n{1}'.format(self.name, [str(i) for i in self.dialogs])

if __name__ == '__main__':
    # create intentions for training
    intent1 = Intent('#comprimentar', ['Olá tudo bem', 'Como está', 'Oieee', 'Oi tudo bem?', 'Bom dia, como vai?', 'tudo bem', 'Eae blz'])
    intent2 = Intent('#despedir', ['Até mais', 'Chau', 'Xauu', 'Até breve', 'Adeus', 'Se falamos mais tarde', 'Fui até', 'estrou indo nessa'])
    intent3 = Intent('#quem_sou_eu', ['Quem é você', 'O que faz?', 'Como se chama?', 'Como é seu nome?', 'Quem o criou', 'Quantos anos?', 'como você é'])
    intent4 = Intent('#comprar', ['Quero um produto', 'Quanto custa?', 'Como consigo comprar?', 'Está na promoção', 'Gostei desse produto', 'quero comprar'])

    # creating answers to intentions
    dialog1 = Dialog(['Oi tudo bem como posso te ajudar?'], '#comprimentar')
    dialog2 = Dialog(['Sou o Jarvis um Bot criando por Anderson Vinicius.'], '#quem_sou_eu')
    dialog3 = Dialog(['Xau bom ter conversado com você.'], '#despedir')
    dialog4 = Dialog(['Vi que esta querendo comprar um produto!'], '#comprar')

    # create bot
    jarvis = Bot('Jarvis', Engine([intent1, intent2, intent3, intent4]))

    # add dialogs supported by bot
    jarvis.add_dialog(dialog1)
    jarvis.add_dialog(dialog2)
    jarvis.add_dialog(dialog3)
    jarvis.add_dialog(dialog4)

    # talking with the bot
    jarvis.dialog('Olá tudo bem com você?')
    jarvis.dialog('Quem és tu?')
    jarvis.dialog('Até mais se nos falamos mais tarde')
    jarvis.dialog('Gostei do produto para cabelo!')
    jarvis.dialog('Boa noite vc está bem?')
    jarvis.dialog('Está tarde fui')
    jarvis.dialog('O que vc faz da vida?')
