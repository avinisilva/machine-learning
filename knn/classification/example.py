import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier

inputs = np.genfromtxt('classification/dataset.data', delimiter=',', usecols=(0, 1, 2))
outputs = np.genfromtxt('classification/dataset.data', delimiter=',', usecols=(3))

x_train, x_test, y_train, y_test = train_test_split(inputs, outputs, test_size=0.3)

knn = KNeighborsClassifier(n_neighbors=14, p=2)

knn.fit(x_train, y_train)
labels = knn.predict(x_test)

percent = 100 * (labels == y_test).sum() / len(x_test)

print(percent)