import math
import numpy as np


def euclidean_dist(p, q):
    p, q = np.array(p), np.array(q)
    diff = p - q
    return math.sqrt(np.dot(diff, diff))

v1 = [2.5, 1, 5]
v2 = [2, 1.8, 1]

print('%.2f' % euclidean_dist(v1, v2))
