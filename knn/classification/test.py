import math


def euclidean_distance(v1, v2):
    dimension, _sum = len(v1), 0
    for index in range(dimension - 1):
        _sum += math.pow(v1[index] - v2[index], 2)

    return math.sqrt(_sum)


def info_data_set(elements, verbose=True):
    if verbose:
        print('Total elements: %d' % len(elements))

    _output1, _output2 = 0, 0
    for element in elements:
        if element[-1] == 1:
            _output1 += 1
        else:
            _output2 += 1

    if verbose:
        print('Total output 1: %d' % _output1)
        print('Total output 2: %d' % _output2)


def load_data_set():
    elements = []
    with open('dataset_sol.data', 'r') as file:
        for line in file.readlines():
            attribute = line.replace('\n', '').split(',')
            elements.append([
                # ceu
                int(attribute[0]),
                # tempo
                int(attribute[1]),
                # vento
                int(attribute[2]),
                # humidade
                int(attribute[3]),
                # mar
                int(attribute[4]),
                # saida
                int(attribute[5]),
            ])
    return elements


def knn(train, new_element, K):
    dists, train_size = {}, len(train)
    # calc distance
    for index in range(train_size):
        distance = euclidean_distance(train[index], new_element)
        dists[index] = distance

    # get nearest neighbors
    k_nearest_neighbors = sorted(dists, key=dists.get)[:K]

    # classifier
    _class1, _class2 = 0, 0
    for index in k_nearest_neighbors:
        if train[index][-1] == 1:
            _class1 += 1
        else:
            _class2 += 1

    if _class1 > _class2:
        return 1
    return 2

elements = load_data_set()
train, test = elements, [[3, 2, 1, 2, 1, 2]]

info_data_set(elements)
print('1 se irá sair sol e 2 se não: %d' % knn(train, test[0], K=1))