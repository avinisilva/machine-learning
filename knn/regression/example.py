from sklearn.neighbors import KNeighborsRegressor
from sklearn.model_selection import train_test_split
from sklearn.datasets import load_boston

boston = load_boston()
# print(boston.DESCR)
# print(boston.data.shape)

knn = KNeighborsRegressor(n_neighbors=9)
knn.fit(boston.data, boston.target)

print(boston.target[0])
print(knn.predict([boston.data[0]]))
