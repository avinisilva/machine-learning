import numpy

array = numpy.array([10, 20, 30, 40, 50])

print(array)
print(type(array))

mat = numpy.array([[10, 15],
                   [20, 25]])

print(mat)
print(mat[0][0])
print(mat[-1][-1])
print(mat[:, 1])
print(mat[1, :])
print(mat[:, 0])
print(mat.transpose())
print(mat + numpy.array([[10, 15], [20, 25]]))
print(mat - numpy.array([[10, 15], [20, 25]]))
print(mat * numpy.array([[10, 15], [20, 25]]))
print(mat.sum())
print(mat.argmax())
print(mat.argmin())
print(mat.mean())
print(mat.diagonal())
# dimension
print(mat.ndim)

#count = 0
#for item in range(1, 100000001):
#    count += item
#print(count)

count = numpy.arange(1, 100000001).sum()
print(count)

l = numpy.array([10, 20, 30, 40])
print(l[1:3])
print(l[::2])
print(l[1:])
print(l[:])
c = l.copy()
c[:] = 5
print(c)
print(l)

my_data = numpy.arange(0,20)
print(my_data)
my_mat = numpy.reshape(my_data, (5, 4))
print(my_mat)
print(my_mat.item(5))

m1 = numpy.array([[1, 2, 3], [4, 5, 6]])
m2 = numpy.array([[7, 8, 9], [1, 2, 3]])
print(m1 / m2)
print(numpy.matrix.round(m1 / m2))
print(m1 - 1)

ages = numpy.array([2, 18, 45, 78, 9])
print(numpy.insert(ages, 0, 1))
print(numpy.append(ages, 10))

mat = numpy.array([[10, 15],
                   [20, 25]])
# vertical
print(mat.sum(axis=0))
# horizontal
print(mat.sum(axis=1))
print(numpy.insert(mat, 0, 1, axis=0))
print(numpy.insert(mat, 0, 1, axis=1))

arr = numpy.array([10, 11, 12, 23])
mat = numpy.array([[10, 11, 12, 23], [14, 78, 23, 92], [19, 45, 62, 95]])
print(numpy.delete(arr, 0))
print(numpy.delete(mat, 0, 1))
print(numpy.array_split(mat, 3, axis=1))

print(numpy.zeros((5, 5)))
print(numpy.ones((5, 5)))
print(numpy.eye(4))

numpy.random.shuffle(arr)
print(arr)

# boolean indexing
print(mat[mat > 23])

v1, v2, v3 = numpy.loadtxt('numbers.txt', skiprows=1, unpack=True)
print(v1, v2, v3)

print(numpy.unique(numpy.array([1, 2, 2, 4, 5, 8, 4, 5, 7])))

