import pandas as pd
import pydataset

serie = pd.Series([1, 2, 3, 4, 5, 6, 7, 8, 9, 5])
print(serie)
print(serie.describe())
print(serie.duplicated())
print(serie.append(serie))

data_frame = pd.DataFrame([
    ['fchollet/keras', 11302],
    ['openai/universe', 4350],
    ['pandas-dev/pandas', 8458]
], columns=['repository', 'stars'])
print(data_frame)
print(data_frame['repository'])
print(data_frame['stars'])
print(data_frame['stars'].describe())
print(data_frame.shape)
print(data_frame.head(2))

titanic = pydataset.data('titanic')
print(titanic.head())
print(titanic.tail())
print(titanic.describe())
print(titanic['class'].value_counts())
